
public class InvalidNeighborIndexException extends RuntimeException{
	/**
	   * Sets up this exception with an appropriate message.
	   * @param collection String representing the name of the collection
	   */
	  public InvalidNeighborIndexException (char character)
	  {
	    super ("The " + character + " is detected in the file.");
	  }
}
