
public class InvalidNeighbourIndexException extends RuntimeException{
	/**
	   * Sets up this exception with an appropriate message.
	   * @param index is the index that the user is trying to access
	   */
	  public InvalidNeighbourIndexException ( int index)
	  {
	    super ("Invalid index " + index);
	  }
}
