
public class UnknownLabyrinthCharacterException extends RuntimeException{
	/**
	   * Sets up this exception with an appropriate message.
	   * @param character  is the character in the maze which is not one that we already have defined as hextypes
	   */
	  public UnknownLabyrinthCharacterException (char character)
	  {
	    super ("The " + character + " is detected in the file.");
	  }
}
