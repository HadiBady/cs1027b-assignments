/**
 * @author HADI BADAWI
 * 
 * the purpose of this class is to find the End or Exit in a hexagon based maze.
 * this class only has a main method, just to execute the algorithm to solve the maze
 * 
 * */

// importing the FileNotFoundException and IOException
import java.io.FileNotFoundException;
import java.io.IOException;

public class SearchForExit {
	
	public static void main(String[] args) throws UnknownLabyrinthCharacterException, FileNotFoundException, IOException {
		
		// creating variables to use in the algorithm
		ArrayStack hexStack = new ArrayStack(); // Using the ArrayStack implementation to create the Stack to add the hexagons to it
		boolean endFound = false; // to determine whether exit is found or not
		int hexStackCounter = 0; // this is how i am counting the number of steps it takes to find exit
		Labyrinth labyrTest = null; // will be used to open, save, and perform certain tasks to the labyrinth
		Hexagon hexagonStart = null; // variable to get the first hexagon
		Hexagon currentHex = null; // the current hexagon which we will be using to performs actions on its neigbhours and itself 
		
		
		// Try statement to ensure that the possible exceptions are dealt with
		try {
			labyrTest = new Labyrinth(args[0]);
		}
		catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			System.exit(1);
		}
		
		catch (IOException e) {
			System.out.println("Invalid input/output");
			System.exit(1);
		}
		
		catch (NullPointerException e) {
			System.out.println("You did not specify a file that exists.");
			System.exit(1);
		}

		
		// If statement to ensure that the maze has a start hexagon
		if (labyrTest.getStart() != null) {
			hexagonStart = labyrTest.getStart();
			hexStack.push(hexagonStart);
		}
		
		else {
			System.out.println("the Labyrinth is missing a start hexagon");
			System.exit(1);
		}		

		
		//While loop so that we do not exit unless exit is found or stack equal to 0
		while (!(endFound) && hexStack.size() != 0) {
				
				// updating counter
				hexStackCounter++;
		
				// pop the top of the stack and set that to currentHex to perform processes on it
				currentHex = (Hexagon) hexStack.pop();
			
				// checks to see if the current hexagon is the end hexagon
				if (currentHex.isEnd()){
					endFound = true;
				}			

				// only want to enter the else statement if the exit is not found
				else {
					
					// try statement to catch invalid neigbhour indexes
					try {
					
						// a for loop to test all of the possible 6 neigbhours of the current hexagon
						for (int i = 0; i <6; i++) {
						
							// conditions to make sure to only add hexagons that exist, are not a wall, and are not visited
							if (currentHex.getNeighbour(i) != null && !(currentHex.getNeighbour(i).isWall()) && (currentHex.getNeighbour(i).isUnvisited())) {
							
								// push the hexagon to the Stack to analyze later
								hexStack.push(currentHex.getNeighbour(i));
								// need to set to pushed type 
								currentHex.getNeighbour(i).setPushed();
							}
						}		
					}	
					
					// want to stop the program from closing if a wrong index is placed
					catch (InvalidNeighbourIndexException i) {
						System.out.println(i.getMessage());
					}
				}
				
				// now that we have finished with the current hexagon, we set it to processed
				currentHex.setProcessed();
				
				// labyrnith needs to be re-updated
				labyrTest.repaint();
		  }

	// if statement to print whether the end was found or not
	if (endFound) {
		System.out.println("The end was found");
	}
	else {
		System.out.println("The end was not found");
	}
	
	// printing the number of steps we went through and if anything is still on the stack
	System.out.format("The number of steps that it took to finish was %s%n", hexStackCounter);
	System.out.format("There were %s tiles on the stack%n", hexStack.size());
	
	// this is to process and save the maze after searching through it
	labyrTest.saveLabyrith(String.format("processed_%s", args[0]));
	}
}
