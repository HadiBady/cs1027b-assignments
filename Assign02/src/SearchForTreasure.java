/**
 * @author HADI BADAWI
 * 
 * the purpose of this class is to find the the total number of hexagons and treasures in the hexagon based maze.
 * this class only has a main method, just to execute the algorithm to go through the maze
 * 
 * */

// importing the exceptions
import java.io.FileNotFoundException;
import java.io.IOException;


public class SearchForTreasure {
	
	// the main method which will throw the exceptions when needed
	public static void main(String[] args) throws UnknownLabyrinthCharacterException, FileNotFoundException, IOException {
		
		// variables to be used later on
		String file = ""; // variable to grab the file name
		LinkedStack hexStack = new LinkedStack(); // the stack that will have the hexagons on it
		int numTiles = 0; // the total number of tiles in the maze
		int numTreasure = 0; // the number of treasures in the  maze
		Labyrinth labyrTest = null; // the variable for creating the maze
		Hexagon hexagonStart = null; // the variable to get the first hexagon
		Hexagon currentHex = null; // the variable to carry the current hexagon

		
		// first assigning the file variable to the file name of the txt for the maze
		file = args[0];

		// try statement to ensure that we can create a maze, and throws appropriate exceptions when required.
		try {
			labyrTest = new Labyrinth(file);
		} 
		
		// need to add catches, you need to go from most specific to least specific, FileNotFoundException is more specific than IOException
		
		catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			System.exit(1);

		}
		
		catch (IOException e){
			System.out.println("Invalid input/output");
			System.exit(1);
		}
		
		catch (NullPointerException e) {
			System.out.println("You did not specify a file that exists.");
			System.exit(1);
		}
		
		
		// grabbing the start hexagon and pushing that to the stack
		hexagonStart = labyrTest.getStart();
		hexStack.push(hexagonStart);
		
		// while loop to ensure that we only leave this loop when no more possible tiles can be examined
		while (hexStack.size()>0) {
			
			// pop the top of the stack, and that becomes our current hexagon
			currentHex = (Hexagon) hexStack.pop();
			numTiles++;

			// if condition to see if the treasure has been found or not
			if (currentHex.hasTreasure()) {
				numTreasure++;	
			}
			
			// try statement to catch if an invalid neighbour index is used and user attempts to access that neighbour
			try {
				// for loop to check all possible neighbours of the current hexagon
				for (int i = 0; i <6; i++) {
					// to ensure that neighbour exists, is unvisisted, and is not a wall.
					if (currentHex.getNeighbour(i) != null && currentHex.getNeighbour(i).isUnvisited() && !(currentHex.getNeighbour(i).isWall())) {
						// pushes the neigbhour to the stack and sets it to type pushed
						hexStack.push(currentHex.getNeighbour(i));
						currentHex.getNeighbour(i).setPushed();
					}
			
				}
			}
			
			catch (InvalidNeighbourIndexException i) {
				System.out.println(i.getMessage());
			}
			
			// now that we have done what we need with the current hexagon, we set it to processed
			currentHex.setProcessed();

			// repaints the maze to show the update for the maze
			labyrTest.repaint();
		}
		
		// Need to print the number of tiles in the maze and how many treasures are in the maze
		System.out.format("Number of tiles in labyrinth: %s%n", numTiles);
		System.out.format("Amount of treasure found: %s%n",  numTreasure);
	
		// saving the maze to a file
		labyrTest.saveLabyrith("processed_" + file);
	
	}
}

