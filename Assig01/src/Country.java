	/**
	 * 
	 * @author HADI BADAWI
	 * purpose of this class is to create a Country class where information about the countires is stored
	 *
	 */


public class Country {
	
	/* @param name is the country name
	 * @param population is the population of the country
	 * @param area is the area of the country
	 * @param continent is the continent in which the country is located
	 * */
	private String name;
	private int population;
	private double area;
	private String continent;


	/* * Constructor that  sets the name, population, area, and continent to the appropriate instance variable*/ 
	public Country(String name, int population, double area, String continent) { 
		this.name = name;
		this.population = population;
		this.area = area;
		this.continent = continent;
		}
	
	
	/* * Constructor in case of creating a country object without the required values*/ 
	public Country() { 
		this.name = "";
		this.population = 0;
		this.area = 0.0;
		this.continent = ""; 
		}
	
	// returns the name of the country
	public String getName() {
		return this.name;
	}
	
	// returns the population of the country
	public int getPopulation() {
		return this.population;
	}
	
	// returns the area of the country
	public double getArea() {
		return this.area;
	}
	
	// returns the continent of the country
	public String getContinent() {
		return this.continent;
	}
	
	// calculates and returns the population density for that country
	public double getPopDensity() {
		return (((double) this.population)/ this.area);
	}
	
	// sets the population for a country
	public void setPopulation(int pop) {
		this.population = pop;
	}
	
	// string representation of the country
	public String toString() {
		return (this.name + " in " + this.continent);
	}
	
}

