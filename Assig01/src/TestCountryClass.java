/**
 * This class tests the Country class defined in SU18 CS1027 Asn1
 * It runs through each of the methods specified in turn based
 * on sample data taken from data.txt.
 * 
 * @author beth
 *
 */
public class TestCountryClass {

	public static void main(String[] args) {

		final int NUM_TEST_COUNTRIES = 5;
		final double EPSILON = 1E-6;
		String[] testData_Countries = { "Canada", "Indonesia", "Nigeria", "Mexico", "Egypt" };
		int[] testData_Populations = { 34207000, 260581100, 173600000, 128632004, 93383574 };
		double[] testData_Areas = { 9976140.0, 1809590.97, 912134.45, 1969230.76, 1000000.00 };
		String[] testData_Continents = { "North America", "Asia", "Africa", "North America", "Africa" };
		// double[] solution_PopDensity = { }
		Country basicTest;

		// We will learn more about "exceptions" later.  Basically java has a way to
		// catch when there are serious/program-ending type errors happening, so you have a
		// chance to recover without the program just crashing.  
		// That's what the try/catch here is about
		
		// Test whether a Country can be created at all
		try {
			basicTest = new Country("Canada", 34207000, 9976140.0, "North America");
		} catch (Exception e) {
			System.out.println("Could not create country. Can't test anything...exiting");
			System.exit(1);
		}

		for (int i = 0; i < NUM_TEST_COUNTRIES; i++) {
			System.out.println("Testing: " + testData_Countries[i]);
			Country test = new Country(testData_Countries[i], testData_Populations[i], testData_Areas[i],
					testData_Continents[i]);

			// ===== Test the basic getter method ========================== //
			System.out.print("\tGetters: ");
			if (test.getName().equals(testData_Countries[i]))
				System.out.print("=");
			else
				System.out.print("X");
			if (test.getPopulation() == testData_Populations[i])
				System.out.print("=");
			else
				System.out.print("X");
			if (test.getArea() == testData_Areas[i])
				System.out.print("=");
			else
				System.out.print("X");
			if (test.getContinent().equals(testData_Continents[i]))
				System.out.print("=");
			else
				System.out.print("X");
			System.out.println(); // == End of test getter methods ========== //

			// ===== Test special getter ==================================== //
			System.out.print("\tPopulation density: ");
			// What we expect
			double popDens = ((double) testData_Populations[i]) / testData_Areas[i];
			// Test with fp precision considerations
			if (Math.abs(test.getPopDensity() - popDens) < EPSILON)
				System.out.print("=");
			else
				System.out.print("X");
			System.out.println(); // === End of test special getter methods = //

			// ===== Test setter ============================================ //
			System.out.print("\tSet population to 0: ");
			test.setPopulation(0);
			if (test.getPopulation() == 0)
				System.out.print("=");
			else
				System.out.print("X");
			System.out.println(); // === End test setter ===================== //

			// ===== Test toString =========================================== //
			System.out.print("\ttoString: ");
			if (test.toString().equals(testData_Countries[i] + " in " + testData_Continents[i]))
				System.out.print("=");
			else
				System.out.print("X");
			System.out.println(); // == End of test toString ================ //
		}
		
		System.out.println("Bye!");
	}
}
