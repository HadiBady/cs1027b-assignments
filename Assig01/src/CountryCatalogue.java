/**
 * The purpose of this class is to create the CountryCatalogue Class to store countries, 
 * search through them, delete or add countries, perform certain tasks, and most importantly,
 * save the new catalog into a text file. 
 * 
 * @author HADI BADAWI
 * 
 * */


// importing the required "modules" for map and hashmap 
import java.util.Map;
import java.util.HashMap;

public class CountryCatalogue {

	//initializing variables and constants to be used later on
	private int countryNum = 0;
	private int tempNum = 0;
	private final int DEFAULT_ARRAY_LENGTH = 10;
	
	//initializing the catalogue and continentMap
	private Country catalogue[] = new Country[DEFAULT_ARRAY_LENGTH];
	private HashMap<String,String> continentMap=new HashMap<String,String>();
	
	//initializing the constants to determine success or failure
	final int SUCCESS = 0;
	final int FAILURE = -1;


	/** * Constructor reads through 2 files, creates an array of default size and stores country information from both files into that array and the hashmap*/ 
	public CountryCatalogue(String countryFileName, String continentFileName) { 

		//int status = FAILURE;
		int i = 0; // incrumentor to ensure that the first time through the files, it skips the headers
		ThingToReadFile reader2 = new ThingToReadFile(continentFileName); // creates a new object which will read through the file
		ThingToReadFile reader = new ThingToReadFile(countryFileName); // creates a new object which will read through the file

		//to ensure that the headers are read through but nothing is done to them
		if (i == 0) {
			String line = reader.readLine();
			String line2 = reader2.readLine();
		}
		
		//loop to read each line, split it, add those items to an array, convert the required items to int or double, then add to the database
		while (! reader.endOfFile() && ! reader2.endOfFile()){
			String line = reader.readLine();
		    String line2 = reader2.readLine();

		    String[] tokens2 = line2.split(",");
		    String[] tokens = line.split(",");
		    
		    String tempPop = tokens2[1].replace(",", "");
		    int pop = Integer.parseInt(tempPop);
		    String tempArea = tokens2[2].replace(",", "");
		    double area = Double.parseDouble(tempArea);
		    
		    Country tempToken = new Country(tokens[0], pop, area, tokens[1]);
		    
		    // this is to make sure that the catalogue array is big enough to store the next item
		    if (countryNum == this.catalogue.length) {
		    	this.catalogue = expandCapacity(this.catalogue);
		    }
		    
		    this.catalogue[i] = tempToken;
		    this.continentMap.put(tokens[0], tokens[1]);
		    i++;
		    countryNum++;
		  }

		// closing the files
		reader.close();
		reader2.close();
	}
	
	
	// addCountry method adds a new country to the catalogue and continentMap and returns true if successful, false if not
	public boolean addCountry(String countryName, int countryPopulation, double countryArea, String countryContinent) {
		boolean result = true; // to ensure that only 1 new country type is in the catalogue 
		Country tempCountry = new Country(countryName, countryPopulation, countryArea, countryContinent); // creating the country object
		
		// for loop to check if the country already exists
		for (int i = 0; i < countryNum; i++) {
			if (this.catalogue[i].getName().equals(countryName) && result && this.continentMap.containsKey(countryName)) {
				result = false;
			}
		}
		
		// to expand capacity of the catalogue if required
		if (this.catalogue.length == countryNum) {
			expandCapacity(this.catalogue);
		}
		
		//if the country does not exist, then this condition is executed
		if (result == true) {
			this.catalogue[countryNum] = tempCountry;
			this.continentMap.put(countryName, countryContinent);
			countryNum++;
		}
		
		return result;
	}
	
	// deleteCountry method deletes the country from the catalogue and continentMap
	// prints whether operation was successful or not
	public void deleteCountry(String countryName) {
		int result;
		
		// checks to see if the country is in the continentMap
		if (continentMap.containsKey(countryName)) {
			continentMap.remove(countryName);
			
			// for loop to see if the country is in catalogue, if it is, 
			// it sets the last object in array to that indexes, then nulls 
			// the last object, and reduces the country count by 1
			for (int i = 0; i < countryNum; i++)
				if (this.catalogue[i].getName().equals(countryName)) {
					this.catalogue[i] = this.catalogue[countryNum - 1];
					this.catalogue[i] = null;
					countryNum--;
				}
			
			// result of the remove of the country
			result = SUCCESS;
		}
		else {
			result = FAILURE;
		}
		
		// condition to print out the corresponding message for successful or unsuccessful deletion
		if (result == SUCCESS) {
			System.out.println("The country was successfully deleted");
		}
		else if (result == FAILURE){
			System.out.println("The country was unsuccessfully deleted");
		}
		else {
			System.out.println("Something is wrong");
		}
	}
	
	
	// findCountry method tries to look to see if the country is in the database
	// returns the country if found, if not, it returns null
	public Country findCountry(String countryName) {		
		boolean test = false; // to tell if country is found or not
		Country tempCountry = null; // storing the country object
		
		//check if the country is in the continentMap
		if (this.continentMap.containsKey(countryName)) {
			test = true;
		}
		
		// goes through array to see if country is in the cataloge
		for (int i = 0; i < countryNum; i++) {
			if (this.catalogue[i].getName().equals(countryName) && test) {
				tempCountry =  this.catalogue[i];
			}
		}
		
		// condition to return the country if found, null otherwise
		if (test == true) {
			return tempCountry;
		}

		else {
			return null;
		}
	}
	
	// printCountryCatalogue method is to print the countries in the catalogue
	public void printCountryCatalogue() {
		if (this.catalogue.length > 0) {
			for (int w = 0; w < countryNum; w++)
				System.out.println(this.catalogue[w]);
		}
	}
	
	// filterCountriesByContinent method creates a tempArray which stores the countries that match the continent, then printis it out
	public void filterCountriesByContinent(String continentName) {	
		String [] tempArray = new String[DEFAULT_ARRAY_LENGTH];
		int tempNum = 0;
		
		//for loop to add countries with matching continent to tempArray
		for (int i = 0; i < countryNum; i++) {
			// checks that there is enough room to add other countries
			if (tempNum == tempArray.length) {
				expandCapacity(tempArray);
			}
			// condition to determine if there is a match for continent given and countinent of a country
			if (this.catalogue[i].getContinent().equals(continentName)) {
				tempArray[tempNum] = this.catalogue[i].getName();
				tempNum++;
				}
		}
		// for loop to print out the countries in the tempArray
		for (int w = 0; w < tempNum; w++) {
			System.out.println(tempArray[w]);
		}
	}
	
	
	// setPopulationOfACountry modifies the country's population if found, returns true if successful, false otherwise
	public boolean setPopulationOfACountry(String countryName, int countryPopulation) {
		//checking if the country is in continentMap to make the process faster
		if (continentMap.containsKey(countryName)){
			//for loop to find matching country
			for (int i = 0; i < countryNum; i++) {
				if (this.catalogue[i].getName().equals(countryName)) {
					this.catalogue[i].setPopulation(countryPopulation);
					return true;
				}
			}
		}
		return false;
	}
	
	// findCountryWithLargestPop method returns the name of the country with the largest population
	public String findCountryWithLargestPop() {
		int largestPop = 0;
		String largestPopName = "";
		
		
		for (int i = 0; i < countryNum; i++) {
			// first has to make a reference for population, thus setting the first instance to the first country in the catalogue
			if (i == 0) {
				largestPop = this.catalogue[i].getPopulation();
				largestPopName = this.catalogue[i].getName();
			}
			// checking to see if the current country in catalogue is greater in population to previous largestPop value
			if (this.catalogue[i].getPopulation() > largestPop) {
				largestPop = this.catalogue[i].getPopulation();
				largestPopName = this.catalogue[i].getName();
			}
		}
		return largestPopName;
	}
	
	// findCountryWithSmallestArea methods returns the country with the smallest area
	public String findCountryWithSmallestArea() {
		double smallestArea = 0;
		String smallestAreaName = "";
				
			for (int i = 0; i < countryNum; i++) {
				// first has to make a reference for area, thus setting the first instance to the first country in the catalogue
				if (i == 0) {
					smallestArea = this.catalogue[i].getArea();
					smallestAreaName = this.catalogue[i].getName();
					}
				// condition to see if current country in the catalogue at i is smaller in area than the value in smallestArea
				if (this.catalogue[i].getArea() < smallestArea) {
					smallestArea = this.catalogue[i].getArea();
					smallestAreaName = this.catalogue[i].getName();
					}
				}
		return smallestAreaName;
		
	}
	
	// filterCountriesByPopDensity method prints out the countries with a population density within the lower and upper bound
	public void filterCountriesByPopDensity(double lowerBound, double upperBound) {
		for (int i = 0; i < countryNum; i++) {
			if (this.catalogue[i].getPopDensity() >= lowerBound && this.catalogue[i].getPopDensity() <= upperBound) {
				System.out.println(this.catalogue[i].getName());
				}
		}
	}
	
	// printMostPopulousContinent method prints the continent with the greatest population
	public void printMostPopulousContinent() {
		HashMap<String, Integer> tempHashMap=new HashMap<String,Integer>();	
		int tempCount = 0;
		Integer newPop = 0;
		Integer tempPop = 0;
		
		// going through the map continentMap through a for loop
		for(Map.Entry<String, String> entry:continentMap.entrySet()){
		     String tempContinent = entry.getValue(); 
		     String tempKey = entry.getKey(); 
		     
		     // for loop to go through catalogue
		     for (int i = 0; i < countryNum; i++) {
		    	 // checking to see if the country is in catalogue 
		    	 if (tempKey.equals(this.catalogue[i].getName())) {
		    		 tempPop = this.catalogue[i].getPopulation();
		    		 String value = continentMap.get(tempKey);
		    		 // the main point of this condition is to make sure that to skip in case where null is stored in catalogue
		    		 if (value != null) {
						newPop = tempPop;
						// this creates a new key for the tempHashMap with continent as key and the value as the population, if it already contains the key,
						// it gets that value and adds the newPop to it as a new key, if not, it throws a nullException which just adds the newPop to the new key.
						tempHashMap.put(tempContinent, tempHashMap.containsKey(tempContinent) ? tempHashMap.get(tempContinent) + newPop: newPop);
		    		 }
		    	 }
		     }
				
		}
		String mostPopCont = "";
		int popMaxCont = 0;
		
		// for loop to go through tempHashMap
		for(Map.Entry<String, Integer> test:tempHashMap.entrySet()){ 
			int tempValue2 = test.getValue(); 
		    String tempKey2 = test.getKey(); 
		    
		    //when comparing values from the database, best to make the first reference set to the first value
			if (tempCount == 0) {
				mostPopCont = tempKey2;
				popMaxCont = tempValue2;
				tempCount++;
			}
			
			// comparing the current tempValue2 to the value of popMaxCont
			if (tempValue2 > popMaxCont) {
				mostPopCont = tempKey2;
				popMaxCont = tempValue2;
			}
		}
				
		// condition to make sure to print out only if you have more than 0 countries in database
		if (tempCount > 0) {
			System.out.println(String.format("The most populous continent is %s, at a population of %s.", mostPopCont, popMaxCont));
		}
	}
	
	
	// saveCountryCatalogue method saves the catalogue to a text document with any filename
	public int saveCountryCatalogue(String filename) {
		ThingToWriteFile writer = new ThingToWriteFile(filename); //creating the ThingToWriteFile object which will handle the writing to the file
		int iFinal = 0; // counter for the number of lines written
		
		for(int i = 0; i < countryNum; i++) {
			double tempPopDens = this.catalogue[i].getPopDensity();			
			writer.writeLine(String.format("%s|%s|%s|%.2f%n", this.catalogue[i].getName(), this.catalogue[i].getContinent(),this.catalogue[i].getPopulation(), tempPopDens) );
			iFinal ++;
		}
		writer.close();
		return iFinal;
	}
	
	// expandCapacity method for Country[], returns a new array with larger capacity
	private Country[] expandCapacity(Country[] oldArray) {
		// creating a new temporary larger array
		Country largerList[] = new Country[oldArray.length* 2];
		
		//adding all the previous items to it
		for (int i = 0; i < oldArray.length; i++) {
			largerList [i] = oldArray[i];
			}
		
		// returning that new array
		return largerList;
	}
	
	// expandCapacity method for String[], returns a new array with larger capacity
	private String[] expandCapacity(String[] oldArray) {
		// creating a new temporary larger array
		String largerList[] = new String[oldArray.length* 2];
		
		//adding all the previous items to it
		for (int i = 0; i < oldArray.length; i++) {
			largerList [i] = oldArray[i];
			}
		// returning that new array
		return largerList;
	}
}
