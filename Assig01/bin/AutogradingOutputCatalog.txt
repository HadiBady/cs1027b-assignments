Grade Report
=====================================
   Testing Country Catalog Class
=====================================
The = indicate correct behaviour was observed
The X indicates incorrect behaviour
There should be a Summary at the bottom with your base grade
=====================================
=====================================
Testing printCountryCatalog

Brazil in South America
Canada in North America
China in Asia
Colombia in South America
Egypt in Africa
France in Europe
Indonesia in Asia
Italy in Europe
Japan in Asia
Nigeria in Africa
Mexico in North America
South Africa in Africa
South Korea in Asia
United States of America in North America
Mostus Populus in Testanica
Small Areaty in Testanica

= Found all expected countries in output
=====================================
Testing add, delete and find
= Found Canada
= Added England (according to return value)
= Found England
Attempting to delete Brazil
X Exception when Testing add, delete, find
	null
=====================================
  Testing filter by continent
X Exception when testing filter by continent
	null
=====================================
  Testing set population
X Exception when testing
	null
=====================================
  Testing find largest population
X Exception when testing largest population
	null
=====================================
  Testing find smallest area
X Exception when testing smallest area
	null
=====================================
  Testing filter by pop density
Testing with range 0, 20 - Expect NO results
X Exception when testing filter by pop dens
	null
=====================================
  Testing print most populous continent
X Exception when testing print most populus continent
	null
=====================================
  Testing save country catalogue
X Exception when testing save country catalogue
	null
=====================================
     Summary
=====================================
10 /10 Print Country Catalogue (and constructor)
2 /10 Add, Delete and Find
2 /10 Filter Countries by Continent
1 /10 Set Population
1 /5 Find Largest Population
1 /5 Find Smallest Area
2 /10 Filter by Population Density
1 /10 Print Most Populous Continent
2 /10 Save Country Catalogue
22.0 / 80 Base Grade For CountryCatalogue.java
=====================================
