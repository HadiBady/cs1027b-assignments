
/**
 * LinkedBinaryTree implements the BinaryTreeADT interface and allows you to find the path to the root for 
 * a element in the tree, find the lowest common ancestor in the tree 
 * 
 * 
 * @author Dr. Lewis
 * @author Dr. Chase
 * @author Hadi Badawi (editor)
 * 
 * @version 1.0, 8/19/08
 */

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class LinkedBinaryTree<T> implements BinaryTreeADT<T> {
	protected int count;
	protected BinaryTreeNode<T> root;

	/**
	 * Creates an empty binary tree.
	 */
	public LinkedBinaryTree() {
		count = 0;
		root = null;
	}

	/**
	 * Creates a binary tree with the specified element as its root.
	 *
	 * @param element
	 *            the element that will become the root of the new binary tree
	 */
	public LinkedBinaryTree(T element) {
		count = 1;
		root = new BinaryTreeNode<T>(element);
	}

	/**
	 * Constructor creates tree from element as root and two subtrees as left and
	 * right subtrees of root.
	 * 
	 * @param element
	 *            element at root
	 * @param leftSubtree
	 *            left subtree
	 * @param rightSubtree
	 *            right subtree
	 */

	public LinkedBinaryTree(T element, LinkedBinaryTree<T> leftSubtree, LinkedBinaryTree<T> rightSubtree) {
		root = new BinaryTreeNode<T>(element);
		count = 1;
		if (leftSubtree != null) {
			count = count + leftSubtree.size();
			root.left = leftSubtree.root;
		} else
			root.left = null;

		if (rightSubtree != null) {
			count = count + rightSubtree.size();
			root.right = rightSubtree.root;
		} else
			root.right = null;

	}

	/**
	 * Returns a reference to the element at the root
	 *
	 * @return a reference to the specified target
	 * @throws EmptyCollectionException
	 *             if the tree is empty
	 */
	public T getRoot() throws EmptyCollectionException {
		// left as programming project
		return root.getElement();
	}

	/**
	 * Returns true if this binary tree is empty and false otherwise.
	 *
	 * @return true if this binary tree is empty
	 */
	public boolean isEmpty() {
		return count == 0;
	}

	/**
	 * Returns the integer size of this tree.
	 *
	 * @return the integer size of this tree
	 */
	public int size() {
		return count;
	}

	/**
	 * Returns true if this tree contains an element that matches the specified
	 * target element and false otherwise.
	 *
	 * @param targetElement
	 *            the element being sought in this tree
	 * @return true if the element in is this tree
	 * @throws ElementNotFoundException
	 *             if an element not found exception occurs
	 */
	public boolean contains(T targetElement) {
		try {
			T junk = this.find(targetElement);
		} catch (ElementNotFoundException e) {
			return false;
		}
		return true;
	}

	/**
	 * Returns a reference to the specified target element if it is found in this
	 * binary tree. Throws a NoSuchElementException if the specified target element
	 * is not found in the binary tree.
	 *
	 * @param targetElement
	 *            the element being sought in this tree
	 * @return a reference to the specified target
	 * @throws ElementNotFoundException
	 *             if an element not found exception occurs
	 */
	public T find(T targetElement) throws ElementNotFoundException {
		BinaryTreeNode<T> current = findAgain(targetElement, root);

		if (current == null)
			throw new ElementNotFoundException("binary tree");

		return (current.element);
	}

	/**
	 * Returns a reference to the specified target element if it is found in this
	 * binary tree.
	 *
	 * @param targetElement
	 *            the element being sought in this tree
	 * @param next
	 *            the element to begin searching from
	 */
	private BinaryTreeNode<T> findAgain(T targetElement, BinaryTreeNode<T> next) {
		if (next == null)
			return null;

		if (next.element.equals(targetElement))
			return next;

		BinaryTreeNode<T> temp = findAgain(targetElement, next.left);

		if (temp == null)
			temp = findAgain(targetElement, next.right);

		return temp;
	}

	/**
	 * Returns a string representation of this binary tree.
	 *
	 * @return a string representation of this binary tree
	 */
	public String toString() {
		Iterator<T> temp = iteratorInOrder();
		StringBuilder toReturn = new StringBuilder();
		while (temp.hasNext())
			toReturn.append(temp.next());
		return toReturn.toString();

	}

	/**
	 * Performs an inorder traversal on this binary tree by calling an overloaded,
	 * recursive inorder method that starts with the root.
	 *
	 * @return an in order iterator over this binary tree
	 */
	public Iterator<T> iteratorInOrder() {
		ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
		inorder(root, tempList);

		return tempList.iterator();
	}

	/**
	 * Performs a recursive inorder traversal.
	 *
	 * @param node
	 *            the node to be used as the root for this traversal
	 * @param tempList
	 *            the temporary list for use in this traversal
	 */
	protected void inorder(BinaryTreeNode<T> node, ArrayUnorderedList<T> tempList) {
		if (node != null) {
			inorder(node.left, tempList);
			tempList.addToRear(node.element);
			inorder(node.right, tempList);
		}
	}

	/**
	 * Performs an preorder traversal on this binary tree by calling an overloaded,
	 * recursive preorder method that starts with the root.
	 *
	 * @return an pre order iterator over this tree
	 */
	public Iterator<T> iteratorPreOrder() {
		// left as a programming project
		return null;
	}

	/**
	 * Performs a recursive preorder traversal.
	 *
	 * @param node
	 *            the node to be used as the root for this traversal
	 * @param tempList
	 *            the temporary list for use in this traversal
	 */
	protected void preorder(BinaryTreeNode<T> node, ArrayUnorderedList<T> tempList) {
		// left as programming project
	}

	/**
	 * Performs an postorder traversal on this binary tree by calling an overloaded,
	 * recursive postorder method that starts with the root.
	 *
	 * @return a post order iterator over this tree
	 */
	public Iterator<T> iteratorPostOrder() {
		// left as programming project
		return null;
	}

	/**
	 * Performs a recursive postorder traversal.
	 *
	 * @param node
	 *            the node to be used as the root for this traversal
	 * @param tempList
	 *            the temporary list for use in this traversal
	 */
	protected void postorder(BinaryTreeNode<T> node, ArrayUnorderedList<T> tempList) {
		// left as programming project

	}

	/**
	 * Performs a levelorder traversal on this binary tree, using a templist.
	 *
	 * @return a levelorder iterator over this binary tree
	 */
	public Iterator<T> iteratorLevelOrder() {
		// left as programming project
		return null;

	}

	/**
	 * @author Hadi Badawi
	 * 
	 * creates an iterator that allows you to iterate to a location in the binary tree
	 * 
	 * @return an pathToRoot iterator over this binary tree
	 * @throws ElementNotFoundException 	when the targetElement is not found in the Binary Tree
	 */

	public Iterator<T> pathToRoot(T targetElement) throws ElementNotFoundException {
		// stack to hold the elements to which they will have the path to root
		LinkedStack<BinaryTreeNode<T>> pathToRootStack = new LinkedStack<BinaryTreeNode<T>>();
		// a temporary list for which we will return after turning it to an iterator for the path
		// from the targetElement to the root.
		ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
		// A hashmap that stores the element as the key, and the node as the value
		// purpose is to store which elements had their right child checked
		HashMap<T, BinaryTreeNode<T>> HashMapR = new HashMap<T, BinaryTreeNode<T>>();
		// A hashmap that stores the element as the key, and the node as the value
		// purpose is to store which elements had their left child checked
		HashMap<T, BinaryTreeNode<T>> HashMapL = new HashMap<T, BinaryTreeNode<T>>();


		// this is to determine if the targetElement is in the tree
		// if not, it will throw the ElementNotFoundException
		T current = find(targetElement);
		 
		// pushing the root to allow the search for the targetElement
		pathToRootStack.push(root);
		
		// calling helper method to find the path to the root from the targetElement
		pathToRootAgain(targetElement, pathToRootStack, HashMapR, HashMapL);
		
		// adding the elements from the stack to the list
		if (!pathToRootStack.isEmpty()) {
			for (int i = (pathToRootStack.size() - 1); i >= 0; i--) {
				tempList.addToRear(pathToRootStack.pop().getElement());
			}
		}
		// returning the iterator version of that list
		return tempList.iterator();
	}

	/**
	 * @author Hadi Badawi
	 * 
	 * Performs the comparisons and checks if the targetElement is in the tree, this is the helper method
	 * for the pathToRoot method 
	 * 
	 * @param targetElement
	 *            is the element which we are trying to find the match for
	 * @param stack
	 *            is the stack that will hold the elements in the proper order to get from the targetElement to the Root
	 *            of the binary tree
	 * @param HashMapR
	 *            is the HashMap to store whether the right child of a node was
	 *            checked
	 * @param HashMapL
	 *            is the HashMap to store whether the left child of a node was
	 *            checked
	 */

	protected void pathToRootAgain(T targetElement, LinkedStack<BinaryTreeNode<T>> stack,
			HashMap<T, BinaryTreeNode<T>> HashMapR, HashMap<T, BinaryTreeNode<T>> HashMapL) {
		
		// this will hold the current element 
		BinaryTreeNode<T> current;

		if (!stack.isEmpty()) {
			// obtaining the element from the stack
			current = (BinaryTreeNode<T>) stack.peek();
			
			// condition to determine if the targetElement matches the current element
			// if so, return without adding anything to the stack
			if (targetElement.equals(current.getElement())) {
				return;
			}

			// checking to see if the current element was already examined or not
			if (!HashMapR.containsKey(current.getElement()) && !HashMapL.containsKey(current.getElement())) {
				
				// since this is the first time the current element is examined, it will be placed in the
				// hashmapL to indicate that this element has been examined
				HashMapL.put(current.getElement(), current);
				
				// if the current element has a left child, we need to add them to the stack
				if (current.left != null) {
					stack.push(current.left);
				}
				// call the function again to check the left child we just added
				pathToRootAgain(targetElement, stack, HashMapR, HashMapL);
			}

			// checking to see if the element is only in the HashMapL and not the HashMapR 
			else if (HashMapL.containsKey(current.getElement()) && !HashMapR.containsKey(current.getElement())) {
				// since this is the second time the current element is examined, it will be placed in the
				// hashmapR to indicate that this element has been examined
				HashMapR.put(current.getElement(), current);
				
				// add this element to the stack to examine only if it exists
				if (current.right != null) {
					stack.push(current.right);
				}
				
				// call the function again to check the right child of current element
				pathToRootAgain(targetElement, stack, HashMapR, HashMapL);
			}
			// if the current element is in both the HashMapR and HashMapL, which means both children were visited
			// and the targetElement is not in their children
			else if (HashMapR.containsKey(current.getElement()) && HashMapL.containsKey(current.getElement())) {
				
				// remove the element on the top of the stack
				stack.pop();
				
				// calling the function to check the new top of the stack
				pathToRootAgain(targetElement, stack, HashMapR, HashMapL);
			}

		}
	}

	/**
	 * @author Hadi Badawi
	 * 
	 * lowestCommonAncestor method finds the lowest common ancestor for 2 elements within a binary tree
	 * 
	 * @param targetOne			is first element in the binary tree 
	 * @param targetTwo			is the second element in the binary tree
	 * @return					returns the element that is the common ancestor of the two target
	 * @throws ElementNotFoundException		if the either element is not in the binary tree
	 */
	
	public T lowestCommonAncestor(T targetOne, T targetTwo) throws ElementNotFoundException {
		// 2 iterators which will hold the path to root for both targets		
		// and the exception is thrown here if either element does not exist
		Iterator<T> targetOneIter = pathToRoot(targetOne);
		Iterator<T> targetTwoIter = pathToRoot(targetTwo);

		// stacks to hold each the path from root to each targetElement
		LinkedStack<T> tempStackOne = new LinkedStack<T>();
		LinkedStack<T> tempStackTwo = new LinkedStack<T>();

		// variable to hold the lowestCommonAncestor
		T lowestComAnc = null;

		// pushing the elements from the iterator to a stack
		while (targetOneIter.hasNext()) {
			T currentOne = targetOneIter.next();
			tempStackOne.push(currentOne);

		}
		// pushing the elements from the iterator to a stack
		while (targetTwoIter.hasNext()) {
			T currentOne = targetTwoIter.next();
			tempStackTwo.push(currentOne);

		}

		// while loop to compare elements, and only change lowestComAncestor if a new match is found
		while (!tempStackOne.isEmpty() && !tempStackTwo.isEmpty()) {
			T currentOne = tempStackOne.pop();
			T currentTwo = tempStackTwo.pop();
			if (currentOne.equals(currentTwo)) {
				lowestComAnc = currentOne;
			}
		}

		// returning the lowest common ancestor
		return lowestComAnc;
	}

	
	/**
	 * @author Hadi Badawi
	 * 
	 * shortestPath finds the shortest path to get from one element in the binary tree to the next
	 * 	
	 * 
	 * @param targetOne							is the starting point for the shortest path
	 * @param targetTwo							is the ending point of the shortest path
	 * @return 									an iterator for the shortest path from one target to another
	 * @throws ElementNotFoundException			is thrown if either target is not found
	 */
	
	public Iterator<T> shortestPath(T targetOne, T targetTwo) throws ElementNotFoundException {
		// 2 iterators which will hold the path to root for both targets		
		// and the exception is thrown here if either element does not exist		
		Iterator<T> targetOneIter = pathToRoot(targetOne);
		Iterator<T> targetTwoIter = pathToRoot(targetTwo);
		
		// this list will have the path from targetOne to LowestCommonAncestor (inclusive)
		ArrayUnorderedList<T> tempListToReturnPart1 = new ArrayUnorderedList<T>();

		// this list will have the path from targetTwo to lowestCommonAncestor
		ArrayUnorderedList<T> tempListToReturnPart2 = new ArrayUnorderedList<T>();

		// this list will have the path from targetOne to targetTwo
		ArrayUnorderedList<T> tempListToReturnComplete = new ArrayUnorderedList<T>();
		
		// the lowest common ancestor for the two targets
		T lowestComAnc = lowestCommonAncestor(targetOne, targetTwo);
		
		// while loop to add elements from targetOne to and including lowestComAnc 
		while (targetOneIter.hasNext()) {
			//temp variable to hold the next element in the iterator
			T iterTemp = targetOneIter.next();
			tempListToReturnPart1.addToRear(iterTemp);
			
			if (iterTemp.equals(lowestComAnc)) {
				// do not want the other elements in the iterator if they will 
				// not aid in resulting in the shortest path
				break;
			}
		}

		// while loop to add elements from lowestComAnc to targetTwo
		while (targetTwoIter.hasNext()) {
			//temp variable to hold the next element in the iterator
			T iterTemp = targetTwoIter.next();
			if (iterTemp.equals(lowestComAnc)) {
				// do not want the other elements in the iterator if they will 
				// not aid in resulting in the shortest path
				break;
			}
			// only adding after the if condition to ensure that lowestComAnc is not added twice
			tempListToReturnPart2.addToFront(iterTemp);		
		}
		
		
		// 2 new iterators which will be added to the final list which will be returned as an iterator
		Iterator<T> iter1 = tempListToReturnPart1.iterator();
		Iterator<T> iter2 = tempListToReturnPart2.iterator();

		// while loops to iterator through the iterator for the path from TargetOne to lowestComAnc
		while (iter1.hasNext()) {
			T iterTemp = iter1.next();
			tempListToReturnComplete.addToRear(iterTemp);

		}
		// while loops to iterator through the iterator for the path from lowestComAnc to targetTwo
		while (iter2.hasNext()) {
			T iterTemp = iter2.next();
			tempListToReturnComplete.addToRear(iterTemp);
		}

		// returning the iterator version of the list which contains the shortest path for 2 targets
		return tempListToReturnComplete.iterator();
	}
}

